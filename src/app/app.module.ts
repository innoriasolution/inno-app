import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {PresentComponent} from './present/present.component';

import {routing} from './app.routes';

@NgModule({
	declarations: [
		AppComponent,
		PresentComponent
	],
	imports: [
		BrowserModule,
		routing
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
