import {Component, OnInit} from '@angular/core';
import { trigger, state, animate, transition, style } from '@angular/animations';


import {Parse} from 'parse';
import {MenuItem} from "../menu-item";

@Component({
	selector: 'app-present',
	templateUrl: './present.component.html',
	styleUrls: ['./present.component.scss']
})
export class PresentComponent implements OnInit {

	products: Array<object> = [];
	time = 5000;
	screen;
	onscreen;

	constructor() {
	}

	ngOnInit() {
		Parse.initialize('7CcP3xJLZgAgSjcarYct8M4h00CkJOifS4aEmHjk');
		Parse.serverURL = 'http://192.168.1.26:1337/';

		this.getProducts();
	}

	getProducts() {
		var Product = Parse.Object.extend('Product');

		var query = new Parse.Query(Product);
		query.equalTo('type', false);
		query.equalTo('inmenu', true);
		query.notContainedIn('categoryid', ['ZBcGormzvq', '8UshBRAkuu', 'nwuvYowVwj', 'YBGp2SAPip', 'k2HMq79HSB', 'Y4tKJ2TzCo']);
		query.limit(1000);

		this.testxxx(query).then((response: Array<object>) => {
			let firstarr = [];
			let secondarr = [];

			let counter:number = 0;
			// if (response.length > 0) {
				for (let obj of response) {
					let item: MenuItem = new MenuItem(counter + 1, obj);

					let index = counter % 6;

					if (index < 3 ) {
						firstarr.push(item);
					} else {
						secondarr.push(item);
					}

					counter++;
				}
			// }

			let temp: Array<object> = [];
			temp.push(firstarr);
			temp.push(secondarr);

			this.products = temp;

			this.screen = Math.ceil(response.length / 6) - 1;
			this.onscreen = 0;

			setInterval(() => {
				if (this.onscreen >= this.screen) {
					this.onscreen = 0;
				} else {
					this.onscreen++;
				}

				window.scrollTo(window.innerWidth * this.onscreen, 0);
			}, this.time);

		}).catch((ex) => {
			console.error(ex);
		});
	}

	testxxx(query) {
		return new Promise((resolve, reject) => {
			query.find({
				success: function (results) {
					resolve(results);
				},
				error: function (error) {
					reject(error);
				}
			});
		});
	}
}
