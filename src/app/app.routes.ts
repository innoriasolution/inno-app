// ====== ./app/app.routes.ts ======

// Imports
// Deprecated import
// import { provideRouter, RouterConfig } from '@angular/router';
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresentComponent } from './present/present.component';

// Route Configuration
export const routes: Routes = [
	// { path: 'dashboard', component: LoginComponent },
	{ path: 'present', component: PresentComponent },

	// otherwise redirect to home
	{ path: '**', redirectTo: 'present' }
];

// Deprecated provide
// export const APP_ROUTER_PROVIDERS = [
//   provideRouter(routes)
// ];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
