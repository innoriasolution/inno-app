export class MenuItem {
	index: number;
	content: object;

	constructor(index: number, content: object) {
		this.index = index;
		this.content = content;
	}
}
